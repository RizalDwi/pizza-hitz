var loading = "<img src='loading_image/loading.gif' style='height:330px;width:40%;display:block;margin-left:auto;margin-right:auto'>";
var loading_dual_ring = "<img src='loading_image/loading_dual_ring.gif' style='height:90px;width:47%;display:block;margin-left:auto;margin-right:auto'>";
var list_menu = [];
var id_hapus_menu = 0;
var tipe_hapus = "";
//var list_menu_paket = [];

function show_menu_all(token) {
    var act = "show_all_menu";
    $('#div_menu').html(loading);
    $("a").removeClass("active");
     $("#all_menu").addClass("active");
    $.post(act, {
            _token: token
           
        },
        function (data) {
            $('#div_menu').html(data);
        });

}
function show_menu(token, jenis_produk, id_active) {
    var act = "show_menu";
    $('#div_menu').html(loading);
    $("a").removeClass("active");
    $("#"+id_active).addClass("active");
    $.post(act, {
            _token: token,
            jenis_produk: jenis_produk
           
        },
        function (data) {
            // var x = document.getElementsByClassName("");
            // x.setAttribute("id","");

            $('#div_menu').html(data);
           // $(".pagination a").attr("data-value","select");
        });

}
function filter_jenis_produk(token, jenis_produk)
{
    var act = "filter_produk";
    $('#body_data').html(loading_dual_ring);
    $.post(act, {
            _token: token,
            jenis_produk: jenis_produk
           
        },
        function (data) {
            $('#body_data').html(data);
            $('#isi_jenis_produk').val(jenis_produk);
        });
    
}
function add_best_seller(token, id, jenis_produk)
{
    var act = "add_best_seller";
    $.post(act, {
            _token: token,
            id: id
           
        },
        function (data) {
            if (data == "1"){
                bootbox.alert("Data telah tersimpan");
                filter_jenis_produk(token, jenis_produk)
            }
        });
    
}

function show_modal_paket(token) {
    var act = "show_modal_paket";
    console.log('c');
    $('#modal_paket').html(loading_dual_ring);
    $.post(act, {
            _token: token
           
        },
        function (data) {
            $('#modal_paket').html(data);
        });

}
function show_modal_edit_menu(token, id){
    var act = "show_modal_edit_menu";
    $('#modal_paket').html(loading_dual_ring);
    $.post(act, {
            _token: token,
            id : id
           
        },
        function (data) {
            $('#modal_paket').html(data);
        });
}

function show_modal_edit_promo(token, id){
    var act = "show_modal_edit_promo";
    $('#modal_paket').html(loading_dual_ring);
    $.post(act, {
            _token: token,
            id : id
           
        },
        function (data) {
            $('#modal_paket').html(data);
        });
}

function tambah_menu_ke_paket(id_menu, nama_menu, jumlah){

    var cek = cek_menu(id_menu);
    var target = "div_menu" + id_menu.toString();
    var target_input = "input_hidden" + id_menu.toString();
    if (cek == 0 || list_menu.length == 0){
        list_menu.push(id_menu);
        $("#btn"+id_menu).html('Batalkan');
        $("#input"+id_menu).prop('disabled', true);
        var input = document.createElement("input");
        input.type = "hidden";
        input.name = "nama_menu[]";
        input.value = id_menu + "|" + jumlah;
        input.setAttribute("id","input_hidden"+id_menu);
        document.getElementById("div_input_hidden").appendChild(input);
        var div_menu = document.createElement("div");
        div_menu.setAttribute("id","div_menu"+id_menu);
        div_menu.className = "col-md-3";
        document.getElementById("div_list_menu_paket").appendChild(div_menu);
        var lbl = document.createElement("label");
        lbl.innerText = nama_menu + " " + jumlah;
        lbl.setAttribute("id","label"+id_menu);
        document.getElementById("div_menu" + id_menu).appendChild(lbl);
        var tbl = document.createElement("button");
          tbl.setAttribute("id", "btndelete"+id_menu);
          tbl.className = "btn";
          
          tbl.onclick = function(){

                del(target, id_menu, target_input);
          };
          document.getElementById(target).appendChild(tbl);
          var icn = document.createElement("i");
          icn.className = "fas fa-window-close";
          document.getElementById("btndelete"+id_menu).appendChild(icn);
    } else {
        del(target, id_menu, target_input);
        $("#btn"+id_menu).html('Tambahkan');
        $("#input"+id_menu).prop('disabled', false);
    }

    
}

function del(id_element, id_menu, id_input){
    console.log(id_input);
    var obj = document.getElementById(id_element);
    obj.remove();
    var input_obj = document.getElementById(id_input);
    input_obj.remove();
    for (var i = 0; i < list_menu.length ; i++) {
        if (list_menu[i] == id_menu){
            delete list_menu[i];
            break;
        }
    }

}

function cek_menu(menu){
    var result;
    for (var i = 0; i < list_menu.length ; i++) {
        if (list_menu[i] == menu){
            delete list_menu[i];
            result = 1;
            break;
        }
        else{
            result = 0;
        }
    }
    return result;
}

function cek_menu_modal(menu){
    var result = 0 ;
    for (var i = 0; i < list_menu.length ; i++) {
        if (list_menu[i] == menu){
            result = 1;
            break;
        }
        else{
            result = 0;
        }
    }
    if (result == 1){
        $("#btn"+menu).html('Batalkan');
        $("#input"+menu).prop('disabled', true);
    } else {
        $("#btn"+menu).html('Tambahkan');
        $("#input"+menu).prop('disabled', false);
    }
    
}


function form_menu() {
    $("a").removeClass("active");
     $("#btn_menu").addClass("active");
    var act = "form_menu";
    $('#div_form').html(loading_dual_ring);
    $.get(act, function(data, status){
        $('#div_form').html(data);
  });

}

function form_paket() {
    $("a").removeClass("active");
     $("#btn_paket").addClass("active");
    var act = "form_paket";
    $('#div_form').html(loading_dual_ring);
    $.get(act, function(data, status){
        $('#div_form').html(data);
  });

}

function form_promo() {
    $("a").removeClass("active");
     $("#btn_promo").addClass("active");
    var act = "form_promo";
    $('#div_form').html(loading_dual_ring);
    $.get(act, function(data, status){
        $('#div_form').html(data);
  });

}

function list_produk_menu() {
   $("a").removeClass("active");
     $("#btn_list_menu").addClass("active");
    var act = "list_produk_menu";
    $('#div_list').html(loading_dual_ring);
    $.get(act, function(data, status){
        $('#div_list').html(data);
        $('#current_menu').val('produk');
  });

}

function list_produk_paket() {
   $("a").removeClass("active");
     $("#btn_list_paket").addClass("active");
    var act = "list_produk_paket";
    $('#div_list').html(loading_dual_ring);
    $.get(act, function(data, status){
        $('#div_list').html(data);
        $('#current_menu').val('paket');
  });
}

function list_promo() {
   $("a").removeClass("active");
     $("#btn_list_promo").addClass("active");
    var act = "list_promo";
    $('#div_list').html(loading_dual_ring);
    $.get(act, function(data, status){
        $('#div_list').html(data);
        $('#current_menu').val('promo');
  });

}

function show_modal_list(token, id){
    var act = "show_modal_list";
    $('#modal_list').html(loading_dual_ring);
    $.post(act, {
            _token: token,
            id : id
           
        },
        function (data) {
            $('#modal_list').html(data);
        });
}

// function add_array_list_menu_paket(isi){
//     list_menu_paket.push(isi);
// }

function get_id(id, tipe){
    id_hapus_menu = id;
    tipe_hapus = tipe;
}

function hapus_produk_menu(token){
    
    var id = id_hapus_menu;
    var tipe = tipe_hapus;
    if (tipe == "promo"){
        var act = "hapus_promo";
    }
    else {
        var act = "hapus_produk_menu";
    }
    //$('#modal_list').html(loading_dual_ring);
    $.post(act, {
            _token: token,
            id : id,
            tipe : tipe
           
        },
        function (data) {
            //$('#modal_list').html(data);
            if (data == "menu"){
                list_produk_menu();
            } else if (data == "paket"){
                list_produk_paket();
            } else {
                list_promo();
            }
        });
}

function display_list_paket() {
    var act = "display_list_paket";
    $('#div_display_paket').html(loading_dual_ring);
    $.get(act, function(data, status){
        $('#div_display_paket').html(data);
  });

}

function fetch_data(page)
 {
  $.ajax({
   url:"fetch_data?page="+page,
   success:function(data)
   {
    $('#div_display_paket').html(data);
   }
  });
 }

 function fetch_data_list_produk(page, jenis_produk)
 {
  $.ajax({
   url:"fetch_data_list_produk?page="+page+"&jenis_produk="+jenis_produk,
   success:function(data)
   {
    $('#body_data').html(data);
    $('#isi_jenis_produk').val(jenis_produk);
   // $(".pagination a").attr("data-value","select");
   }
  });
 }

 function fetch_data_list_paket(page)
 {
  $.ajax({
   url:"fetch_data_list_paket?page="+page,
   success:function(data)
   {
    $('#div_list').html(data);
   // $(".pagination a").attr("data-value","select");
   }
  });
 }

 function fetch_data_promo(page)
 {
  $.ajax({
   url:"fetch_data_promo?page="+page,
   success:function(data)
   {
    $('#div_list').html(data);
   // $(".pagination a").attr("data-value","select");
   }
  });
 }

 function show_isi_pesan(token, id, target){
     $("div").removeClass("active_chat");
    var act = "show_isi_pesan";
    $('#box_pesan').html(loading_dual_ring);
    $.post(act, {
            _token: token,
            id : id
           
        },
        function (data) {
            $('#box_pesan').html(data);
            $("#"+target).addClass("active_chat");
        });
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;

}
