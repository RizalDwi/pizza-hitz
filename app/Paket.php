<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Paket;

class Paket extends Model
{
    protected $table = 'paket_menu';

    public static function list_isi_paket($id){
	$isi = DB::table('paket_has_produk')
                ->join('produk','paket_has_produk.id_produk','produk.id')
                ->select('produk.nama_produk', 'paket_has_produk.id_produk','paket_has_produk.jumlah')
                ->where('paket_has_produk.id_paket', $id)
                ->get();
	return $isi;
 }
}
