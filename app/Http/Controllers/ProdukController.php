<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Paket;
use App\Promo;
use App\Feedback;
use DB;
class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function main(){
        $produk = Produk::where('best_seller', "1")->get();
        $promo = Promo::all();
        return view('main', compact('produk','promo'));
    }

    public function index()
    {
        return view('tambah_produk');
    }

    public function select_all()
    {
        $produk = Produk::all();
        //$jenis_produk = "";
        return view('menu', compact('produk'));
    }
    public function select_menu(Request $request)
    {
       // $jenis_produk = $request->jenis_produk;
        $produk = Produk::where('jenis_produk', $request->jenis_produk)->get();
        return view('menu', compact('produk'));
    }
     public function list_produk()
    {
       // $produk = Produk::all();
        return view('list_produk');
    }
    public function filter_produk(Request $request)
    {
        if ($request->jenis_produk == '1'){
            $produk = Produk::paginate(10);
        }else{
            $produk = Produk::where('jenis_produk', $request->jenis_produk)->paginate(10);
        }
        
        return view('filter_produk', compact('produk'));
    }

    public function add_best_seller(Request $request){
        $produk = Produk::where('id', $request->id)->get();
        $value;
        if ($produk[0]->best_seller == "1"){
            $value = "0";
        }else{
            $value = "1";
        }

        $new_produk = Produk::find($request->id);
        $new_produk->best_seller = $value;
        $new_produk->save();
        echo "1";
    }
    public function show_modal_paket(){
        $produkPizza = Produk::where('jenis_produk', 'Pizza')->get();
        $produkPasta = Produk::where('jenis_produk', 'Pasta')->get();
        $produkDessert = Produk::where('jenis_produk', 'Dessert')->get();
        $produkAppetizer = Produk::where('jenis_produk', 'Appetizer')->get();
        $produkHotDrink = Produk::where('jenis_produk', 'Hot Drink')->get();
        $produkDrink = Produk::where('jenis_produk', 'Drink')->get();
        $produkFreshJuice = Produk::where('jenis_produk', 'Fresh Juice')->get();
        $produkPaketAyam = Produk::where('jenis_produk', 'Paket Ayam')->get();
        $produkFloatMilkshake = Produk::where('jenis_produk', 'Float Milkshake')->get();
        $produk = Produk::all();
        return view('list_menu_dalam_paket', compact('produkPizza','produkPasta','produkDessert','produkAppetizer','produkHotDrink','produkDrink','produkFreshJuice','produkPaketAyam','produkFloatMilkshake','produk'));
    }

    public function add_menu_paket(Request $request){

        $paket = new Paket;
        $paket->nama_paket = $request->nama_paket;
        $paket->harga = $request->harga;
        $paket->deskripsi = $request->deskripsi;
        $paket->save();

        $id_paket = DB::table('paket_menu')->latest('created_at')->first();
        $batas = count($request->nama_menu);
        for ($i=0; $i < $batas; $i++) {
            $arr = explode("|",$request->nama_menu[$i]);
            DB::table('paket_has_produk')->insert(
            ['id_produk' => $arr[0],
            'id_paket' => $id_paket->id,
            'jumlah' => $arr[1]]
            );
        }
        return back()
            ->with('success','Data berhasil disimpan');
    }

    public function show_modal_edit_menu(Request $request){
        $produk = Produk::where('id', $request->id)->get();
        return view('form_edit_menu', compact('produk'));
    }
    public function show_modal_edit_promo(Request $request){
        $promo = Promo::find($request->id);
        return view('form_edit_promo', compact('promo'));
    }

    public function update_menu(Request $request){
       
        
         $new_produk = Produk::find($request->id);
        if (is_null(request()->image)){
            
             $new_produk->nama_produk = $request->nama_produk;
            $new_produk->jenis_produk = $request->jenis_produk;
            $new_produk->harga = $request->harga;
            $new_produk->deskripsi = $request->deskripsi;
            
            $new_produk->save();
        } else {
                request()->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',

            ]);
            $name = time();
            $imageName = $name.'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);

             $new_produk->nama_produk = $request->nama_produk;
            $new_produk->jenis_produk = $request->jenis_produk;
            $new_produk->harga = $request->harga;
            $new_produk->deskripsi = $request->deskripsi;
        
        
            $new_produk->gambar_produk = $name;
            $new_produk->ekstensi = request()->image->getClientOriginalExtension();
            $new_produk->save();
        }
        
        // }

        

        
       

        return back()
            ->with('success','Data berhasil disimpan');
    }

    public function update_promo(Request $request){
       
        
         $new_promo = Promo::find($request->id);
        if (is_null(request()->image)){
            
             $new_promo->nama_promo = $request->nama_promo;
            
            
            $new_promo->save();
        } else {
                request()->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',

            ]);
            $name = time();
            $imageName = $name.'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('/images/promo'), $imageName);

             $new_promo->nama_promo = $request->nama_promo;
        
        
        
            $new_promo->gambar_promo = $name;
            $new_promo->ekstensi = request()->image->getClientOriginalExtension();
            $new_promo->save();
        }
        
        
        return back()
            ->with('success','Data berhasil disimpan');
    }
    public function form_menu(){
        return view('form_tambah_menu');
    }

    public function form_paket(){
        return view('form_tambah_paket');
    }

     public function form_promo(){
        return view('form_tambah_promo');
    }

    public function list_produk_menu(){
        return view('list_produk_menu');
    }

    public function list_produk_paket(){
        $paket = Paket::paginate(10);
        return view('list_produk_paket', compact('paket'));
    }

    public function list_promo(){
        $promo = Promo::paginate(10);
        return view('list_promo', compact('promo'));
    }

    public function modal_list(Request $request){
        $produk = DB::table('paket_has_produk')
                    ->join('produk','paket_has_produk.id_produk','produk.id')
                    ->select('produk.nama_produk','paket_has_produk.jumlah')
                    ->where('paket_has_produk.id_paket', $request->id)
                    ->get();
       // dd($produk);
       return view('modal_list_produk_in_paket', compact('produk'));
    }

    public function form_edit_paket(Request $request)
    {
        $produk = Paket::where('id', $request->id)->get();
        $paket = DB::table('paket_has_produk')
                ->join('produk','paket_has_produk.id_produk','produk.id')
                ->select('produk.nama_produk', 'paket_has_produk.id_produk','paket_has_produk.jumlah')
                ->where('paket_has_produk.id_paket', $request->id)
                ->get();
        return view('form_edit_paket', compact('produk','paket'));
    }

    public function update_menu_paket(Request $request){
        foreach ($request->menu_id as $key) {
            DB::table('paket_has_produk')->where('id_produk', $key)
            ->where('id_paket', $request->id)->delete();
        }
        $paket = Paket::find($request->id);
        $paket->nama_paket = $request->nama_paket;
        $paket->harga = $request->harga;
        $paket->deskripsi = $request->deskripsi;
        $paket->save();

        //$id_paket = DB::table('paket_menu')->latest('created_at')->first();
        $batas = count($request->nama_menu);
        for ($i=0; $i < $batas; $i++) {
            $arr = explode("|",$request->nama_menu[$i]);
            DB::table('paket_has_produk')->insert(
            ['id_produk' => $arr[0],
            'id_paket' => $request->id,
            'jumlah' => $arr[1]]
            );
        }
        // $produk = Paket::where('id', $request->id)->get();
        // $paket = DB::table('paket_has_produk')
        //         ->join('produk','paket_has_produk.id_produk','produk.id')
        //         ->select('produk.nama_produk', 'paket_has_produk.id_produk','paket_has_produk.jumlah')
        //         ->where('paket_has_produk.id_paket', $request->id)
        //         ->get();
        // return view('form_edit_paket', compact('produk','paket'))->with('success','Data berhasil disimpan');;
        return redirect('/list')->with('success','Data berhasil disimpan');
    }

    public function hapus_produk_menu(Request $request){
        DB::table('paket_has_produk')->where('id_produk', $request->id)->delete();
        if ($request->tipe == "menu")
        {
            DB::table('paket_has_produk')->where('id_produk', $request->id)->delete();
            DB::table('produk')->where('id',$request->id)->delete();
        } else {
            DB::table('paket_has_produk')->where('id_paket', $request->id)->delete();
            DB::table('paket_menu')->where('id',$request->id)->delete();
        }
        

        echo $request->tipe;
    }

    public function hapus_promo(Request $request){
        $promo = Promo::find($request->id);
        $promo->delete();
        echo $request->tipe;
    }

    public function display_list_paket(Request $request){
        $paket = Paket::paginate(4);
        return view('display_list_paket', compact('paket'));
    }

    public function fetch_data(Request $request)
    {
     if($request->ajax())
     {
        
      $paket = Paket::paginate(4);
      return view('display_list_paket', compact('paket'))->render();
     }
    }

    public function fetch_data_list_produk(Request $request)
    {
     if($request->ajax())
     {
        if ($request->jenis_produk == '1'){
            $produk = Produk::paginate(10);
        }else{
            $produk = Produk::where('jenis_produk', $request->jenis_produk)->paginate(10);
        }
        
      return view('filter_produk', compact('produk'))->render();
     }
    }

    public function fetch_data_list_paket(Request $request)
    {
     if($request->ajax())
     {

        $paket = Paket::paginate(10);
       return view('list_produk_paket', compact('paket'))->render();
     }
    }

    public function fetch_data_promo(Request $request)
    {
     if($request->ajax())
     {

        $promo = Promo::paginate(10);
       return view('list_promo', compact('promo'))->render();
     }
    }

  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpan_promo(Request $request){
        request()->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',

        ]);
        $name = time();
        $imageName = $name.'.'.request()->image->getClientOriginalExtension();

        request()->image->move(public_path('/images/promo'), $imageName);

        $promo = new Promo;
        $promo->nama_promo = $request->nama_promo;
        $promo->gambar_promo = $name;
        $promo->ekstensi = request()->image->getClientOriginalExtension();
        $promo->save();

        return back()
            ->with('success','Data berhasil disimpan');
    }

    public function simpan_pesan(Request $request){
        $pesan = new Feedback;
        $pesan->nama = $request->nama;
        $pesan->email = $request->email;
        $pesan->pesan = $request->pesan;
        $pesan->save();

        return back()
            ->with('success','Pesan telah terkirim');
    }

    public function store(Request $request)
    {
        request()->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',

        ]);
        $name = time();
        $imageName = $name.'.'.request()->image->getClientOriginalExtension();

       

        request()->image->move(public_path('images'), $imageName);
        $produk = new Produk;
        $produk->nama_produk = $request->nama_produk;
        $produk->harga = $request->harga;
        $produk->jenis_produk = $request->jenis_produk;
        $produk->deskripsi = $request->deskripsi;
        $produk->gambar_produk = $name;
        $produk->ekstensi = request()->image->getClientOriginalExtension();
        $produk->save();
        //  $request->validate([
        //     'image' => 'nullable|mimes:jpeg,jpg,png,gif|max:100000',
        // ]);
        // $file = $request->images('images');
  
        // $imageName = time().'.'.$file->getClientOriginalExtension();  
   
        // $request->image->move(public_path('images'), $imageName);

   
        return back()
            ->with('success','Data berhasil disimpan');

        //  if (request()->hasFile('imagePath')){
        // $uploadedImage = $request->file('imagePath');
        // $imageName = time() . '.' . $image->getClientOriginalExtension();
        // $destinationPath = public_path('/images');
        // $uploadedImage->move($destinationPath, $imageName);
        // $image->imagePath = $destinationPath . $imageName;
    
    }

    public function feedback(Request $request){
        $pesan = Feedback::all();
        return view('feedback', compact('pesan'));
    }

    public function show_isi_pesan(Request $request){
        $pesan = Feedback::find($request->id);
        return view('show_isi_pesan', compact('pesan'));
    }

    public function about(){
        $pesan = Feedback::all();
        return view('about', compact('pesan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
