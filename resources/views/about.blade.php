<!DOCTYPE html>
<html lang="en">
<head>
        <title>Luigi's</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">

        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('luigis/fonts/beyond_the_mountains-webfont.css') }}" type="text/css"/>

        <!-- Stylesheets -->
        

       <link href="{{ asset('luigis/plugin-frameworks/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('luigis/plugin-frameworks/swiper.css') }}" rel="stylesheet">
        <link href="{{ asset('luigis/fonts/ionicons.css') }}" rel="stylesheet">
        <link href="{{ asset('luigis/common/styles.css') }}" rel="stylesheet">

</head>
<body>

    @include('layout.header')


<section class="bg-4 h-500x main-slider pos-relative">
        <div class="triangle-up pos-bottom"></div>
        <div class="container h-100">
                <div class="dplay-tbl">
                        <div class="dplay-tbl-cell center-text color-white pt-90">
                                <h5><b>TERBAIK DI BANGKALAN</b></h5>
                                <h2 class="mt-30 mb-15">Tentang Kami</h2>
                        </div><!-- dplay-tbl-cell -->
                </div><!-- dplay-tbl -->
        </div><!-- container -->
</section>

<section>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d989.9152100987254!2d112.72933177883621!3d-7.049081455835226!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd805028bbe290d%3A0xdddd501c773ae89f!2sPIZZA%20HITS%20BANGKALAN!5e0!3m2!1sid!2sid!4v1595949776745!5m2!1sid!2sid" width="800" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</section>

<section class="story-area bg-seller color-white pos-relative">
        <div class="pos-bottom triangle-up"></div>
        <div class="pos-top triangle-bottom"></div>
        <div class="container">
                <div class="heading">
                        <img class="heading-img" src="images/heading_logo.png" alt="">
                        <h2>Apa yang pelanggan katakan tentang kami</h2>
                </div>

                <div class="swiper-container" data-slide-effect="slide" data-autoheight="false" data-swiper-speed="500" data-swiper-margin="25" data-swiper-slides-per-view="3"
                     data-swiper-breakpoints="true" data-scrollbar="true" data-swiper-loop="true" data-swpr-responsive="[1, 2, 2, 2]">

                        <div class="swiper-wrapper pb-90 pb-sm-60 left-text center-sm-text">
                             @foreach ($pesan as $ps)
                                <div class="swiper-slide">
                                        
                                        <p class="color-ash mb-50 mb-sm-30 mt-20">{{ $ps->pesan }} </p>
                                        
                                        <h6><b class="color-primary">{{ $ps->nama }}</b>,<b class="color-ash">Customer</b>
                                        </h6>
                                </div><!-- swiper-slide -->
                            @endforeach
                                
                        </div><!-- swiper-wrapper -->

                        <div class="swiper-pagination"></div>
                </div><!-- swiper-container -->
        </div><!-- container -->
</section>


<section class="story-area left-text center-sm-text">
        <div class="container">

                <div class="row">
                        <div class="col-md-6"><img class="mb-30" src="images/about-1-600x400.jpg" alt=""></div>
                        <div class="col-md-6"><img class="mb-30" src="images/about-2-600x400.jpg" alt=""></div>
                        <div class="col-md-12">
                                <div class="mt-50 mt-sm-30 mb-50 mb-sm-30 center-text"> <h2>We are Pizza Hits bangkalan</h2> </div>

                                <h3 class="center-text mb-50 mb-sm-30 plr-25">Restoran pizza pertama dibangkalan</h3>
                                <h4 class="center-text mb-50 mb-sm-30 plr-25">Restoran pizza hits bangkalan berdiri pada tanggal 24 januari 2020. Pizza hits bangkalan merupakan restoran pizza pertama yang hadir dikota bangkalan. Dengan menu varia pizza yang bervariasi anda dapat menemukan rasa pizza yang lezat sesuai dengan selera yang anda inginkan.
                                </h4>
                        </div>
                        
                </div><!-- row -->

        </div><!-- container -->
</section>

@include('layout.footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js"></script>
<script src="{{ asset('luigis/plugin-frameworks/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('luigis/plugin-frameworks/bootstrap.min.js') }}"></script>
<script src="{{ asset('luigis/plugin-frameworks/swiper.js') }}"></script>
<script src="{{ asset('luigis/plugin-frameworks/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('luigis/plugin-frameworks/progressbar.min.js') }}"></script>
<script src="{{ asset('luigis/plugin-frameworks/tether.min.js') }}"></script>
<script src="{{ asset('luigis/common/scripts.js') }}"></script>
</body>
</html>