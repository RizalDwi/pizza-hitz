 @foreach ($paket as $pk)
 <div class="col-md-5">
                                <div class="sided-90x mb-30 ">
                                        
                                        
                                        <div class="s-right">
                                                <h5 class="mb-10"><b>{{ $pk->nama_paket }}</b><b class="color-primary float-right">Rp. {{ number_format($pk->harga, 0, ',', '.') }}</b></h5>
                                                <ul class="list-group">
                                                  <?php
                                                    $data_isi = App\Paket::list_isi_paket($pk->id);
                                                  ?>
                                                  @foreach ($data_isi as $di)
                                                  <li class="list-group-item d-flex justify-content-between align-items-center">
                                                    {{ $di->nama_produk }}
                                                    <span class="badge badge-primary badge-pill">{{ $di->jumlah }}</span>
                                                  </li>
                                                  @endforeach
                                                 
                                                </ul>
                                        </div><!--s-right-->
                                </div><!-- sided-90x -->
                        </div>
@endforeach
<div class="col-md-12" style="margin-left: 42%;margin-right: 42%">
    {!! $paket->links() !!}
</div>