<header>
        <div class="container">
                <a class="logo" href="{{ route('landing') }}"><img src="{{ asset('luigis/images/logo_pizza.png') }}" alt="Logo"></a>

                <!-- <div class="right-area">
                        <h6><a class="plr-20 color-white btn-fill-primary" href="#">ORDER: +34 685 778 8892</a></h6>
                </div>< -->

                <a class="menu-nav-icon" data-menu="#main-menu" href="#"><i class="ion-navicon"></i></a>

                <ul class="main-menu font-mountainsre" id="main-menu">
                        <li><a href="{{ route('landing') }}">BERANDA</a></li>
                        @auth
                        <li class="dropdown">
                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <b>MENU ADMIN</b>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{ route('tambah') }}">Tambah Produk</a>
                            <a class="dropdown-item" href="{{ route('list_produk') }}">Daftar Produk</a>
                           
                          </div>
                        </li>
                        @endauth
                        <li><a href="{{ route('about') }}">TENTANG KAMI</a></li>
                        <li><a href="{{ route('feedback') }}">FEEDBACK</a></li>
                        @auth
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">KELUAR</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                       @endauth

                <div class="clearfix"></div>
        </div><!-- container -->
</header>