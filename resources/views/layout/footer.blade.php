<!-- modal -->
  <div class="modal fade bd-example-modal-lg" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Tambahkan menu dalam paket</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal_paket">
        




      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        
      </div>
    </div>
  </div>
</div>
<footer class="pb-50  pt-70 pos-relative">
        <div class="pos-top triangle-bottom"></div>
        <div class="container-fluid">
                <a href="index.html"><img src="{{ asset('luigis/images/logo_pizza.png') }}" alt="Logo"></a>

                <div class="pt-30">
                        <p class="underline-secondary"><b>Address:</b></p>
                        <p>Jl. R. E. Marthadinata Wr 05, RT.004/RW.001, Mlajah, </p>
                        <p>Kec. Bangkalan, Kabupaten Bangkalan, Jawa Timur 69116 </p>
                </div>

                <div class="pt-30">
                        <p class="underline-secondary mb-10"><b>Phone:</b></p>
                        <a href="tel:+6285236510160 ">+6285 236 510 160</a>
                </div>

                <div class="pt-30">
                        <p class="underline-secondary mb-10"><b>Email:</b></p>
                        <a href="mailto:bangkalanpizzahits@gmail.com"> bangkalanpizzahits@gmail.com</a>
                </div>

                <ul class="icon mt-30">
                        <li><a href="https://www.facebook.com/pizzahitz.bangkalan"><i class="ion-social-facebook"></i></a></li>
                        <li><a href="https://instagram.com/pizzahits_bangkalan"><i class="ion-social-instagram"></i></a></li>
                        <li><a href="mailto:bangkalanpizzahits@gmail.com"><i class="icon ion-email"></i></a></li>
                        <li><a href="https://wa.me/+6285236510160"><i class="ion-social-whatsapp"></i></a></li>
                </ul>
        </div><!-- container -->
</footer>