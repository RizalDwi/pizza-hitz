<!DOCTYPE HTML>
<html lang="en">
<head>
@include('layout.include')
</head>
<body>
@include('layout.header')

<section class="bg-6 h-500x main-slider pos-relative">
        <div class="triangle-up pos-bottom"></div>
        <div class="container h-100">
                <div class="dplay-tbl">
                        <div class="dplay-tbl-cell center-text color-white pt-90">
                                
                                <h3 class="mt-30 mb-15">Kirim Ulasan</h3>
                        </div><!-- dplay-tbl-cell -->
                </div><!-- dplay-tbl -->
        </div><!-- container -->
</section>
@auth
<section>
        <div class="container">
<h3 class=" text-center">Messaging</h3>
<div class="messaging">
      <div class="inbox_msg">
        <div class="inbox_people">
          <div class="headind_srch">
            <div class="recent_heading">
              <h4>Recent</h4>
            </div>
            <div class="srch_bar">
              <div class="stylish-input-group">
                <input type="text" class="search-bar"  placeholder="Search" >
                <span class="input-group-addon">
                <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                </span> </div>
            </div>
          </div>
          <div class="inbox_chat">
                @foreach ($pesan as $ps)
        <button type="button" onclick="show_isi_pesan('{{csrf_token()}}','{{ $ps->id }}','chat_list{{ $ps->id }}')">
            <div class="chat_list" id="chat_list{{ $ps->id }}">
              <div class="chat_people">
                <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                <div class="chat_ib">
                  <h5>{{ $ps->nama }} <span class="chat_date">{{ $ps->created_at }}</span></h5>
                  
                </div>
              </div>
            </div>
            </button>
            @endforeach
           
            
            
            
            
           
          </div>
        </div>
        <div class="mesgs" id="box_pesan">
          <div align="center">
                  <p style="font-size: 30px;">Klik salah satu kontak</p>
          </div>
          <!-- <div class="type_msg">
            <div class="input_msg_write">
              <input type="text" class="write_msg" placeholder="Type a message" />
              <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
            </div>
          </div> -->
        </div>
      </div>
      
      
    </div>
</section>
@endauth
@guest
@if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
       
        @endif
  
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


<section class="story-area left-text center-sm-text">
        <div class="container">
                <div class="heading">
                        <img class="heading-img" src="images/heading_logo.png" alt="">
                        <h5 class="mt-10 mb-30">Kirim ulasan tentang kami</h5>
                        <p class="mx-w-700x mlr-auto">Kami sangat menerima kritik dan saran dari pelanggan agar dapat memberikan pelayanan yang terbaik</p>
                </div>

                <form class="form-style-1 placeholder-1" method="POST" action="{{ route('simpan_pesan') }}">
                         {{ csrf_field() }}
                        <div class="row">
                                <div class="col-md-6"> <input required class="mb-20" type="text" placeholder="Nama" name="nama" id="nama">  </div>
                                <div class="col-md-6"> <input class="mb-20" type="email" name="email" id="email" placeholder="E-mail" required>  </div>
                           
                                <div class="col-md-12">
                                        <textarea class="h-200x ptb-20" placeholder="Tulis Pesan" id="pesan" name="pesan" required></textarea></div>
                        </div><!-- row -->
                        <h6 class="center-text mtb-30"><button class="btn-primaryc plr-25" type="submit"><b>SEND MESSAGE</b></button></h6>
                </form>
        </div><!-- container -->
</section>
@endguest

@include('layout.footer')
</body>