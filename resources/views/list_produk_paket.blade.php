<table class="table">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col">No.</th>
                      <th scope="col">Nama</th>
                      <th scope="col">Harga</th>
                      <th scope="col">Daftar Menu dalam Paket</th>
                      <th scope="col">Act</th>
                    </tr>
                  </thead>
                  <tbody id="body_data">
                    <?php
                    $no = 1;
                    $i = $paket->perPage() * ($paket->currentPage() - 1);
                    ?>
                    @foreach ($paket as $pk)
                    <tr>
                      <td>{{$no + $i}}</td>
                      <td>{{ $pk->nama_paket }}</td>
                      <td>Rp. {{ number_format($pk->harga, 0, ',', '.') }}</td>
                      <td><button type="button" onclick="show_modal_list('{{csrf_token()}}','{{ $pk->id }}')" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Detail</button></td>
                      <td><form action="{{ route('form_edit_paket') }}" method="POST" target="_blank">
                        <input type="hidden" name="id" value="{{ $pk->id }}">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary">Edit</button>
                      </form>
                      <button type="button" data-toggle="modal" data-target="#Modal_hapus" class="btn btn-primary" onclick="get_id('{{$pk->id}}','paket')">
                          Hapus
                        </button></td>
                    </tr>
                    <?php
                      $no++
                    ?>
                    @endforeach
                    <tr>
                      <td colspan="5">{!! $paket->links() !!}</td>
                    </tr>
                   
                   
                  </tbody>
                </table>