<form class="form-style-1 placeholder-1" action="{{ route('update_menu') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="{{$produk[0]->id}}">
                        <div class="row">
                                <div class="col-md-12"> <input class="mb-20" value="{{ $produk[0]->nama_produk }}" id="nama_produk" name="nama_produk" type="text" placeholder="Nama Produk">  </div>
                                <div class="col-md-12"> <input class="mb-20" id="harga" value="{{ $produk[0]->harga }}" name="harga" type="number" oninput="$('#rupiah').html(addCommas(this.value));" placeholder="Harga">  
                                        <small>Rp. <span id="rupiah">
                                                0
                                                        </span>
                                                </small>
                                </div>
                                 <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Jenis Produk</label>
                                            <select class="form-control" id="jenis_produk" name="jenis_produk">
                                              <option value="{{ $produk[0]->jenis_produk }}" selected>{{ $produk[0]->jenis_produk }}</option>
                                              <option>-</option>
                                              <option value="Pizza">Pizza</option>
                                              <option value="Pasta">Pasta</option>
                                              <option value="Hot Drink">Hot Drink</option>
                                               <option value="Drink">Drink</option>
                                               <option value="Fresh Juice">Fresh Juice</option>
                                                
                                                 <option value="Dessert">Dessert</option>
                                                  <option value="Appetizer">Appetizer</option>
                                              
                                            </select>
                                          </div>
                                </div>
                                <div class="col-md-12">
                                        <div class="form-group">
                                                <?php
                                                $img = $produk[0]->gambar_produk.".".$produk[0]->ekstensi;
                                                ?>
                                                <label>Gambar Saat Ini</label><br>
                                                
                                                <img src="{{ asset('images/'.$img) }}" id="gambar_produk_edit" alt="">
                                         </div>  
                                 </div>
                                <div class="col-md-12">
                                        <div class="form-group">
                                                <label for="exampleFormControlFile1">Gambar Produk</label>
                                                 <input type="file" class="form-control-file" name="image" id="image">
                                         </div>  
                                 </div>
                                <div class="col-md-12">
                                        <textarea class="h-200x ptb-20" id="deskripsi_produk" name="deskripsi" placeholder="Deskripsi Produk">{{ $produk[0]->deskripsi }}</textarea></div>
                        </div><!-- row -->
                        <h6 class="center-text mtb-30"><button type="submit" class="btn-primaryc plr-25"><b>Simpan</b></button></h6>
                </form>
<script type="text/javascript">
  $('#rupiah').html(addCommas({{ $produk[0]->harga }}))
</script>