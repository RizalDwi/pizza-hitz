<!DOCTYPE HTML>
<html lang="en">
<head>
@include('layout.include')
</head>
<body>
@include('layout.header')
<section class="bg-1 h-900x main-slider pos-relative">
        <div class="triangle-up pos-bottom"></div>
        <div class="container h-100">
                <div class="dplay-tbl">
                        <div class="dplay-tbl-cell center-text color-white">

                                <h5><b>TERBAIK DI BANGKALAN</b></h5>
                                <h1 class="mt-30 mb-15">Pizza Hitz</h1>
                                
                        </div><!-- dplay-tbl-cell -->
                </div><!-- dplay-tbl -->
        </div><!-- container -->
</section>

<section class="story-area left-text center-sm-text pos-relative">
        <div class="abs-tbl bg-2 w-20 z--1 dplay-md-none"></div>
        <div class="abs-tbr bg-3 w-20 z--1 dplay-md-none"></div>
        <div class="container">
                <div class="heading">
                        <img class="heading-img" src="{{ asset('luigis/images/heading_logo.png') }}" alt="">
                        <h2>Promo kami</h2>
                </div>

                <div class="row" id="div_promo">
                        <div class="container">
                  <div class="row justify-content-md-center">
                    <div class="col col-lg-2">
                      
                    </div>
                    <div class="col-md-auto">
                       <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                          <div class="carousel-inner">

                                @foreach ($promo as $key => $prm)
                                <?php
                                        $imgpromo = $prm->gambar_promo.".".$prm->ekstensi;
                                ?>
                                @if ($key == 0)
                            <div class="carousel-item active" data-interval="10000">
                              <img src="{{ asset('images/promo/'.$imgpromo) }}" class="d-block w-100" alt="...">
                            </div>
                            @else
                            <div class="carousel-item" data-interval="10000">
                              <img src="{{ asset('images/promo/'.$imgpromo) }}" class="d-block w-100" alt="...">
                            </div>
                            @endif

                            @endforeach
                            
                          </div>
                          <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
                    </div>
                    <div class="col col-lg-2">
        
                    </div>
                  </div>
                  
                </div>
                        
                      
                        

                </div><!-- row -->
        </div><!-- container -->
</section>



<section class="story-area left-text center-sm-text pos-relative">
        <div class="abs-tbl bg-2 w-20 z--1 dplay-md-none"></div>
        <div class="abs-tbr bg-3 w-20 z--1 dplay-md-none"></div>
        <div class="container">
                <div class="heading">
                        <img class="heading-img" src="{{ asset('luigis/images/heading_logo.png') }}" alt="">
                        <h2>Menu paket kami</h2>
                </div>

                <div class="row" id="div_display_paket">
                       

                </div><!-- row -->
        </div><!-- container -->
</section>


<section class="story-area bg-seller color-white pos-relative">
        <div class="pos-bottom triangle-up"></div>
        <div class="pos-top triangle-bottom"></div>
        <div class="container">
                <div class="heading">
                        <img class="heading-img" src="{{ asset('luigis/images/heading_logo.png') }}" alt="">
                        <h2>Best Sellers</h2>
                </div>

                <div class="row">
                        @foreach ($produk as $pr)
                        <div class="col-lg-3 col-md-4  col-sm-6 ">
                                <div class="center-text mb-30">
                                        <div class="ïmg-200x mlr-auto pos-relative">
                                                <?php
                                                $img = $pr->gambar_produk.".".$pr->ekstensi;
                                                ?>
                                                
                                                <img src="{{ asset('images/'.$img) }}" id="gambar_best_seller" alt="">
                                        </div>
                                        <h5 class="mt-20">{{$pr->nama_produk}}</h5>
                                        <h4 class="mt-5"><b>Rp. {{ number_format($pr->harga, 0, ',', '.') }}</b></h4>
                                        
                                </div><!--text-center-->
                        </div><!-- col-md-3 -->
                        @endforeach

                </div><!-- row -->

               
        </div><!-- container -->
</section>


<section>
        <div class="container">
                <div class="heading">
                        <img class="heading-img" src="{{ asset('luigis/images/heading_logo.png') }}" alt="">
                        <h2>Manu Kami</h2>
                </div>

                <div class="row">
                        <div class="col-sm-12">
                                <ul class="selecton brdr-b-primary mb-70">
                                        <li><a class="active" id="all_menu" href="javascript:void(0)" onclick="show_menu_all('{{csrf_token()}}')"><b>ALL</b></a></li>
                                        <li><a id="menu_pizza" href='javascript:void(0)' onclick="show_menu('{{csrf_token()}}','Pizza','menu_pizza')"><b>PIZZA</b></a></li>
                                        <li><a id="menu_pasta" href='javascript:void(0)' onclick="show_menu('{{csrf_token()}}','Pasta','menu_pasta')"><b>PASTA</b></a></li>
                                        <li><a href='javascript:void(0)' id="menu_hot_drink" onclick="show_menu('{{csrf_token()}}','Hot Drink','menu_hot_drink')"><b>HOT DRINK</b></a></li>
                                        <li><a href='javascript:void(0)' id="menu_drink" onclick="show_menu('{{csrf_token()}}','Drink','menu_drink')"><b>DRINK</b></a></li>
                                        <li><a href='javascript:void(0)' id="menu_jus" onclick="show_menu('{{csrf_token()}}','Fresh Juice','menu_jus')"><b>FRESH JUICE</b></a></li>
                                        <li><a href='javascript:void(0)' id="menu_dessert" onclick="show_menu('{{csrf_token()}}','Dessert','menu_dessert')"><b>DESERTS</b></a></li>
                                        <li><a href='javascript:void(0)' id="menu_appetizer" onclick="show_menu('{{csrf_token()}}','Appetizer','menu_appetizer')"><b>APPETIZER</b></a></li>
                                        <li><a href='javascript:void(0)' id="menu_paketayam" onclick="show_menu('{{csrf_token()}}','Paket Ayam','menu_paketayam')"><b>AYAM</b></a></li>
                                        <li><a href='javascript:void(0)' id="menu_floatmilkshake" onclick="show_menu('{{csrf_token()}}','Float Milkshake','menu_floatmilkshake')"><b>FLOAT & MILKSHAKE</b></a></li>
                                </ul>
                        </div><!--col-sm-12-->
                </div><!--row-->

                

                <div class="row" id="div_menu">
                         
                </div><!-- row -->

                
        </div><!-- container -->
</section>
@include('layout.footer')
</body>
<script type="text/javascript">
        display_list_paket();
        show_menu_all('{{csrf_token()}}');
        $(document).on('click', '.pagination a', function(event){
          event.preventDefault(); 
          var page = $(this).attr('href').split('page=')[1];
          fetch_data(page);
         });

        // $(document).on('click', '#id_select', function(event){
        //   event.preventDefault(); 
        //   var page = $(this).attr('href').split('page=')[1];
        //   fetch_data_select(page);
        //  });
</script>