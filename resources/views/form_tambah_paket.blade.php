<form class="form-style-1 placeholder-1" action="{{ route('simpan_paket') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                                <div class="col-md-12"> <input class="mb-20" id="nama_paket" name="nama_paket" type="text" placeholder="Nama Paket">  </div>
                                <div class="col-md-12" >
                                        <input class="mb-20" id="harga" name="harga" type="number" placeholder="Harga" oninput="$('#rupiah').html(addCommas(this.value));" >
                                                <small>Rp. <span id="rupiah">
                                                0
                                                        </span>
                                                </small>
                                </div>
                                
                                <div class="col-md-12">
                                        <div class="form-group">
                                          <h4>Menu di dalam paket</h4>
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong" onclick="show_modal_paket('{{csrf_token()}}')">Pilih</button>
                                          <div class="row" id="div_list_menu_paket" style="margin: 10px">
                                            

                                          </div>
                                          <div id="div_input_hidden"></div>
                                          
                                                 
                                         </div>  
                                 </div>
                                <div class="col-md-12">
                                        <textarea class="h-200x ptb-20" id="deskripsi_produk" name="deskripsi" placeholder="Deskripsi Produk"></textarea></div>
                        </div><!-- row -->
                        <h6 class="center-text mtb-30"><button type="submit" class="btn-primaryc plr-25"><b>Simpan</b></button></h6>
                </form>