<!DOCTYPE HTML>
<html lang="en">
<head>
@include('layout.include')
</head>
<body>
@include('layout.header')

<section class="bg-6 h-500x main-slider pos-relative">
        <div class="triangle-up pos-bottom"></div>
        <div class="container h-100">
                <div class="dplay-tbl">
                        <div class="dplay-tbl-cell center-text color-white pt-90">
                                <h5><b>EDIT PAKET</b></h5>
                                
                        </div><!-- dplay-tbl-cell -->
                </div><!-- dplay-tbl -->
        </div><!-- container -->
</section>

 

<section class="story-area left-text center-sm-text">
 
          @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
       
        @endif
  
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="container" id="div_form">
               
            <form class="form-style-1 placeholder-1" action="{{ route('update_menu_paket') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $produk[0]->id }}">
                        <div class="row">
                                <div class="col-md-12"> <input class="mb-20" id="nama_paket" name="nama_paket" type="text" placeholder="Nama Paket" value="{{ $produk[0]->nama_paket }}">  </div>
                                <div class="col-md-12"> <input class="mb-20" id="harga" value="{{ $produk[0]->harga }}" name="harga" type="number" placeholder="Harga" oninput="$('#rupiah').html(addCommas(this.value));">
                                  <small>Rp. <span id="rupiah">
                                                0
                                                        </span>
                                                </small>

                                </div>
                                
                                <div class="col-md-12">
                                        <div class="form-group">
                                          <h4>Menu di dalam paket</h4>
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong" onclick="show_modal_paket('{{csrf_token()}}')">Pilih</button>
                                          <div class="row" id="div_list_menu_paket" style="margin: 10px">
                                           

                                          </div>
                                           <div id="div_input_hidden"></div>
                                           <div id="div_id_menu">
                                             @foreach ($paket as $pk)
                                             <input type="hidden" name="menu_id[]" value="{{$pk->id_produk}}">
                                             @endforeach
                                           </div>
                                         
                                                 
                                         </div>  
                                 </div>
                                <div class="col-md-12">
                                        <textarea class="h-200x ptb-20" id="deskripsi_produk" name="deskripsi" placeholder="Deskripsi Produk">{{ $produk[0]->deskripsi }}</textarea></div>
                        </div><!-- row -->
                        <h6 class="center-text mtb-30"><button type="submit" class="btn-primaryc plr-25"><b>Simpan</b></button></h6>
                </form>
               

                
        </div><!-- container -->
</section>




@include('layout.footer')
</body>

<script type="text/javascript">
  $('#rupiah').html(addCommas({{ $produk[0]->harga }}))
  <?php
      foreach ($paket as $key) {
        ?>
          
            tambah_menu_ke_paket('<?php echo $key->id_produk; ?>', '<?php echo $key->nama_produk; ?>', '<?php echo $key->jumlah; ?>');
        <?php
      }
  ?>
</script>