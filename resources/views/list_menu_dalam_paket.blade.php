<h4>Pizza</h4>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col" style="width: 40%">Nama Menu</th>
              <th scope="col" style="width: 40%">Jumlah</th>
              <th scope="col">Act</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $no = 1;
            ?>
            @foreach ($produkPizza as $pz)
            <tr>
              <th scope="row">{{ $no++ }}</th>
              <td>{{ $pz->nama_produk }}</td>
              <td><input type="number" id="input{{ $pz->id }}" style="width: 40px" min="1" value="1" ></td>
              <td><button type="button" id="btn{{ $pz->id }}" class="btn btn-primary btn-sm" onclick="tambah_menu_ke_paket('{{ $pz->id }}','{{ $pz->nama_produk }}',$('#input{{ $pz->id }}').val())">Tambahkan</button></td>
            </tr>
            @endforeach
          

           
          </tbody>
        </table>

<h4>Pasta</h4>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
             <th scope="col" style="width: 40%">Nama Menu</th>
              <th scope="col" style="width: 40%">Jumlah</th>
              <th scope="col">Act</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $no = 1;
            ?>
            @foreach ($produkPasta as $ps)
            <tr>
              <th scope="row">{{ $no++ }}</th>
              <td>{{ $ps->nama_produk }}</td>
              <td><input type="number" id="input{{ $ps->id }}" style="width: 40px" min="1" value="1" name=""></td>
              <td><button type="button" id="btn{{ $ps->id }}" class="btn btn-primary btn-sm" onclick="tambah_menu_ke_paket('{{ $ps->id }}','{{ $ps->nama_produk }}',$('#input{{ $ps->id }}').val())">Tambahkan</button></td>
            </tr>
            @endforeach
          

           
          </tbody>
        </table>

<h4>Dessert</h4>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
            <th scope="col" style="width: 40%">Nama Menu</th>
              <th scope="col" style="width: 40%">Jumlah</th>
              <th scope="col">Act</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $no = 1;
            ?>
            @foreach ($produkDessert as $pd)
            <tr>
              <th scope="row">{{ $no++ }}</th>
              <td>{{ $pd->nama_produk }}</td>
              <td><input id="input{{ $pd->id }}" type="number" style="width: 40px" min="1" value="1" name=""></td>
              <td><button id="btn{{ $pd->id }}" type="button" class="btn btn-primary btn-sm" onclick="tambah_menu_ke_paket('{{ $pd->id }}','{{ $pd->nama_produk }}',$('#input{{ $pd->id }}').val())">Tambahkan</button></td>
            </tr>
            @endforeach
          

           
          </tbody>
        </table>
<h4>Appetizer</h4>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col" style="width: 40%">Nama Menu</th>
              <th scope="col" style="width: 40%">Jumlah</th>
              <th scope="col">Act</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $no = 1;
            ?>
            @foreach ($produkAppetizer as $pa)
            <tr>
              <th scope="row">{{ $no++ }}</th>
              <td>{{ $pa->nama_produk }}</td>
              <td><input id="input{{ $pa->id }}" type="number" style="width: 40px" min="1" value="1" name=""></td>
              <td><button id="btn{{ $pa->id }}" type="button" class="btn btn-primary btn-sm" onclick="tambah_menu_ke_paket('{{ $pa->id }}','{{ $pa->nama_produk }}',$('#input{{ $pa->id }}').val())">Tambahkan</button></td>
            </tr>
            @endforeach
           
          </tbody>
        </table>


<h4>Hot Drink</h4>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col" style="width: 40%">Nama Menu</th>
              <th scope="col" style="width: 40%">Jumlah</th>
              <th scope="col">Act</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $no = 1;
            ?>
            @foreach ($produkHotDrink as $hd)
            <tr>
              <th scope="row">{{ $no++ }}</th>
              <td>{{ $hd->nama_produk }}</td>
              <td><input id="input{{ $hd->id }}" type="number" style="width: 40px" min="1" value="1" name=""></td>
             <td><button id="btn{{ $hd->id }}" type="button" class="btn btn-primary btn-sm" onclick="tambah_menu_ke_paket('{{ $hd->id }}','{{ $hd->nama_produk }}',$('#input{{ $hd->id }}').val())">Tambahkan</button></td>
            </tr>
            @endforeach
          

           
          </tbody>
        </table>

<h4>Drink</h4>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col" style="width: 40%">Nama Menu</th>
              <th scope="col" style="width: 40%">Jumlah</th>
              <th scope="col">Act</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $no = 1;
            ?>
            @foreach ($produkDrink as $pdr)
            <tr>
              <th scope="row">{{ $no++ }}</th>
              <td>{{ $pdr->nama_produk }}</td>
              <td><input id="input{{ $pdr->id }}" type="number" style="width: 40px" min="1" value="1" name=""></td>
             <td><button id="btn{{ $pdr->id }}" type="button" class="btn btn-primary btn-sm" onclick="tambah_menu_ke_paket('{{ $pdr->id }}','{{ $pdr->nama_produk }}',$('#input{{ $pdr->id }}').val())">Tambahkan</button></td>
            </tr>
            @endforeach
          

           
          </tbody>
        </table>

<h4>Fresh Juice</h4>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col" style="width: 40%">Nama Menu</th>
              <th scope="col" style="width: 40%">Jumlah</th>
              <th scope="col">Act</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $no = 1;
            ?>
            @foreach ($produkFreshJuice as $pfj)
            <tr>
              <th scope="row">{{ $no++ }}</th>
              <td>{{ $pfj->nama_produk }}</td>
              <td><input id="input{{ $pfj->id }}" type="number" style="width: 40px" min="1" value="1" name=""></td>
              <td><button id="btn{{ $pfj->id }}" type="button" class="btn btn-primary btn-sm" onclick="tambah_menu_ke_paket('{{ $pfj->id }}','{{ $pfj->nama_produk }}',$('#input{{ $pfj->id }}').val())">Tambahkan</button></td>
            </tr>
            @endforeach
          

           
          </tbody>
        </table>

        <h4>Paket Ayam</h4>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col" style="width: 40%">Nama Menu</th>
              <th scope="col" style="width: 40%">Jumlah</th>
              <th scope="col">Act</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $no = 1;
            ?>
            @foreach ($produkPaketAyam as $ppa)
            <tr>
              <th scope="row">{{ $no++ }}</th>
              <td>{{ $ppa->nama_produk }}</td>
              <td><input id="input{{ $ppa->id }}" type="number" style="width: 40px" min="1" value="1" name=""></td>
              <td><button id="btn{{ $ppa->id }}" type="button" class="btn btn-primary btn-sm" onclick="tambah_menu_ke_paket('{{ $ppa->id }}','{{ $ppa->nama_produk }}',$('#input{{ $ppa->id }}').val())">Tambahkan</button></td>
            </tr>
            @endforeach
          

           
          </tbody>
        </table>

        <h4>Float and Milkshake</h4>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col" style="width: 40%">Nama Menu</th>
              <th scope="col" style="width: 40%">Jumlah</th>
              <th scope="col">Act</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $no = 1;
            ?>
            @foreach ($produkFloatMilkshake as $pfm)
            <tr>
              <th scope="row">{{ $no++ }}</th>
              <td>{{ $pfm->nama_produk }}</td>
              <td><input id="input{{ $pfm->id }}" type="number" style="width: 40px" min="1" value="1" name=""></td>
              <td><button id="btn{{ $pfm->id }}" type="button" class="btn btn-primary btn-sm" onclick="tambah_menu_ke_paket('{{ $pfm->id }}','{{ $pfm->nama_produk }}',$('#input{{ $pfm->id }}').val())">Tambahkan</button></td>
            </tr>
            @endforeach
          

           
          </tbody>
        </table>
 <script type="text/javascript">
   <?php
        foreach ($produk as $key) {
        ?>

        cek_menu_modal(<?php echo $key->id; ?>)

        <?php
        }
   ?>
 </script>