           <?php
                        $no = 1;
                        $i = $produk->perPage() * ($produk->currentPage() - 1);
                    ?>
                    @foreach ($produk as $pr)
                    <tr>
                      <th scope="row">{{ $no + $i }}</th>
                      <td>{{ $pr->nama_produk }}</td>
                      <td>{{ $pr->jenis_produk }}</td>
                      <td>Rp. {{ number_format($pr->harga, 0, ',', '.') }}</td>
                      <?php
                        $cek = ($pr->best_seller == "1")?($cek = "checked"):($checked = "");
                      ?>
                      <td style="text-align: center;"><input type="checkbox" id="exampleCheck1" {{$cek}} onclick="add_best_seller('{{csrf_token()}}', '{{$pr->id}}',$('#jenis_produk').val())"></td>
                      <td><button data-toggle="modal" data-target="#exampleModalLong" type="button" onclick="show_modal_edit_menu('{{csrf_token()}}','{{$pr->id}}')" class="btn btn-primary">Edit</button>
                        <button type="button" data-toggle="modal" data-target="#Modal_hapus" class="btn btn-primary" onclick="get_id('{{$pr->id}}','menu')">
                          Hapus
                        </button>
                      </td>

                    </tr>
                    <?php
                      $no++
                    ?>
                    @endforeach
                    
                    <tr>
                      <td colspan="6">{!! $produk->links() !!}</td>
                      <input type="hidden" id="isi_jenis_produk" value="1">
                    </tr>

