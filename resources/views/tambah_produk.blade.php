<!DOCTYPE HTML>
<html lang="en">
<head>
@include('layout.include')
</head>
<body>
@include('layout.header')

<section class="bg-6 h-500x main-slider pos-relative">
        <div class="triangle-up pos-bottom"></div>
        <div class="container h-100">
                <div class="dplay-tbl">
                        <div class="dplay-tbl-cell center-text color-white pt-90">
                                <h5><b>TAMBAH PRODUK</b></h5>
                                
                        </div><!-- dplay-tbl-cell -->
                </div><!-- dplay-tbl -->
        </div><!-- container -->
</section>

 

<section class="story-area left-text center-sm-text">
  <div class="row">
                        <div class="col-sm-12">
                                <ul class="selecton brdr-b-primary mb-70">
                                        <li><a class="active" id="btn_menu" href="javascript:void(0)" onclick="form_menu()"><b>Tambah Menu</b></a></li>
                                        <li><a id="btn_paket" href='javascript:void(0)' onclick="form_paket()"><b>Tambah Paket</b></a></li>
                                        <li><a id="btn_promo" href='javascript:void(0)' onclick="form_promo()"><b>Tambah Promo</b></a></li>
                                        
                                </ul>
                        </div><!--col-sm-12-->
                </div>
          @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
       
        @endif
  
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="container" id="div_form">
               

               

                
        </div><!-- container -->
</section>
@include('layout.footer')



</body>

<script type="text/javascript">
    form_menu();
</script>