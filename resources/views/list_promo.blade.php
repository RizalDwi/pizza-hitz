<table class="table">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col">No.</th>
                      <th scope="col">Nama Promo</th>
                      <th scope="col">Gambar Promo</th>
                      <th scope="col">Act</th>
                    </tr>
                  </thead>
                  <tbody id="body_data">
                    <?php
                    $no = 1;
                    ?>
                    @foreach ($promo as $pr)
                    <tr>
                      <td>{{$no++}}</td>
                      <td>{{ $pr->nama_promo }}</td>
                      <?php
                            $img = $pr->gambar_promo.".".$pr->ekstensi;
                      ?>
                      <td><img id="gambar_promo" src="{{ asset('images/promo/'.$img) }}"></td>
                      <td><button data-toggle="modal" data-target="#exampleModalLong" type="button" onclick="show_modal_edit_promo('{{csrf_token()}}','{{$pr->id}}')" class="btn btn-primary">Edit</button>
                        <button type="button" data-toggle="modal" data-target="#Modal_hapus" class="btn btn-primary" onclick="get_id('{{$pr->id}}','promo')">
                          Hapus
                        </button></td>
            
                    </tr>
                    @endforeach
                    <tr>
                      <td colspan="5">{!! $promo->links() !!}</td>
                    </tr>
                   
                   
                  </tbody>
                </table>