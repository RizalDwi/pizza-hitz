<!DOCTYPE HTML>
<html lang="en">
<head>
@include('layout.include')
</head>
<body>
@include('layout.header')



<section class="bg-4 h-500x main-slider pos-relative">
        <div class="triangle-up pos-bottom"></div>
        <div class="container h-100">
                <div class="dplay-tbl">
                        <div class="dplay-tbl-cell center-text color-white pt-90">
                                <h5><b>THE BEST IN TOWN</b></h5>
                                <h2 class="mt-30 mb-15">Daftar Produk</h2>
                        </div><!-- dplay-tbl-cell -->
                </div><!-- dplay-tbl -->
        </div><!-- container -->
</section>

 @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
       
        @endif
  
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


<section class="story-area left-text center-sm-text">
  <input type="hidden" id="current_menu" value="produk">
  <div class="row">
                        <div class="col-sm-12">
                                <ul class="selecton brdr-b-primary mb-70">
                                        <li><a class="active" id="btn_list_menu" href="javascript:void(0)" onclick="list_produk_menu();"><b>Daftar Menu</b></a></li>
                                        <li><a id="btn_list_paket" href='javascript:void(0)' onclick="list_produk_paket()"><b>Daftar Paket</b></a></li>
                                         <li><a id="btn_list_promo" href='javascript:void(0)' onclick="list_promo()"><b>Daftar Promo</b></a></li>
                                        
                                </ul>
                        </div><!--col-sm-12-->
                </div>
        <div class="container" id="div_list">
            
                

        </div><!-- container -->
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal_list">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

<!-- modal hapus -->


<!-- Modal -->
<div class="modal fade" id="Modal_hapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah anda yakin akan menghapusnya?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="hapus_produk_menu('{{csrf_token()}}')">Yakin</button>
      </div>
    </div>
  </div>
</div>




@include('layout.footer')
</body>
<script type="text/javascript">
  list_produk_menu();
  
  $(document).on('click', '.pagination a', function(event){
          event.preventDefault(); 
          var page = $(this).attr('href').split('page=')[1];
          if ($('#current_menu').val() == "produk"){
            fetch_data_list_produk(page,$('#isi_jenis_produk').val());
          } else if ($('#current_menu').val() == "paket"){
            fetch_data_list_paket(page);
          } else {
            fetch_data_promo(page);
          }
          
         });
</script>