                      @foreach ($produk as $pd)
                        <div class="col-md-6">
                                <div class="sided-90x mb-30 ">
                                        <?php
                                                $img = $pd->gambar_produk.".".$pd->ekstensi;
                                        ?>
                                        <div class="s-left"><img class="br-3" id="gambar_produk" src="{{ asset('images/'.$img) }}" alt="Menu Image"></div><!--s-left-->
                                        <div class="s-right">
                                                <h5 class="mb-10"><b>{{ $pd->nama_produk }}</b><b class="color-primary float-right">Rp. {{ number_format($pd->harga, 0, ',', '.') }}</b></h5>
                                                <p id="deskripsi" class="pr-70">{{ $pd->deskripsi }}</p>
                                        </div><!--s-right-->
                                </div><!-- sided-90x -->
                        </div><!-- food-menu -->
                        @endforeach
<!-- <div class="col-md-12" style="margin-left: 42%;margin-right: 42%">
    
</div>

                        
<script type="text/javascript">
   
</script> -->