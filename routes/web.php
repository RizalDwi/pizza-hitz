<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/feedback', function () {
//     return view('feedback');
// });
// Route::get('/list', function () {
//     return view('list_produk');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function () {   
    Route::get('/tambah', 'ProdukController@index')->name('tambah');
    Route::post('/simpan','ProdukController@store')->name('simpan');
	Route::post('/simpan_promo','ProdukController@simpan_promo')->name('simpan_promo');

	Route::get('/list','ProdukController@list_produk')->name('list_produk');
	Route::post('/filter_produk','ProdukController@filter_produk');
	Route::post('/add_best_seller','ProdukController@add_best_seller');
	Route::post('/show_modal_paket','ProdukController@show_modal_paket');
	Route::post('/add_menu_paket','ProdukController@add_menu_paket')->name('simpan_paket');
	Route::post('/show_modal_edit_menu','ProdukController@show_modal_edit_menu');
	Route::post('/show_modal_edit_promo','ProdukController@show_modal_edit_promo');
	Route::post('/update_menu','ProdukController@update_menu')->name('update_menu');
	Route::get('/form_menu','ProdukController@form_menu');
	Route::get('/form_paket','ProdukController@form_paket');
	Route::get('/form_promo','ProdukController@form_promo');
	Route::get('/list_produk_menu','ProdukController@list_produk_menu');
	Route::get('/list_produk_paket','ProdukController@list_produk_paket');
	Route::get('/list_promo','ProdukController@list_promo');

	Route::post('/show_isi_pesan','ProdukController@show_isi_pesan');
	Route::post('/show_modal_list', 'ProdukController@modal_list');
	Route::post('/form_edit_paket','ProdukController@form_edit_paket')->name('form_edit_paket');
	Route::post('/update_promo','ProdukController@update_promo')->name('update_promo');

	Route::post('/update_menu_paket', 'ProdukController@update_menu_paket')->name('update_menu_paket');
	Route::post('/hapus_produk_menu','ProdukController@hapus_produk_menu');
	Route::post('/hapus_promo','ProdukController@hapus_promo');
	
	Route::get('/fetch_data_list_produk','ProdukController@fetch_data_list_produk');
	Route::get('/fetch_data_promo','ProdukController@fetch_data_promo');
});

Route::get('/feedback','ProdukController@feedback')->name('feedback');
Route::post('/simpan_pesan','ProdukController@simpan_pesan')->name('simpan_pesan');
Route::get('/fetch_data','ProdukController@fetch_data');
Route::post('/show_all_menu', 'ProdukController@select_all');
Route::post('/show_menu', 'ProdukController@select_menu');


Route::get('/display_list_paket','ProdukController@display_list_paket');

Route::get('/fetch_data_list_paket','ProdukController@fetch_data_list_paket');


Route::get('/','ProdukController@main')->name('landing');

Route::get('/about', 'ProdukController@about')->name('about');

// Route::get('/test', function () {
//     return view('test');
// });